\section{QUBO Formulation for SMP}
For the stable marriage problem manly three things need to be considered:
\begin{enumerate}
	\item No one is supposed to be matched twice \ref{eq:prel_qubo_noone_twice}
	\item The count of matches (edges in the graph) is supposed to be always $n$ \ref{eq:prel_qubo_max_count}
	\item There may not exist blocking pairs \ref{eq:prel_qubo_stab}
\end{enumerate}
\begin{equation} \label{eq:prel_qubo_noone_twice}
	p \cdot \sum_{i} \sum_{j} (i \neq j) \cdot x_{i} \cdot x_{j} \cdot ( (m_i = m_j) + (w_i = w_j) ) = 0
\end{equation}
\begin{equation} \label{eq:prel_qubo_max_count}
	p_{1} \cdot (\sum_{i} x_{i} - n)^2 = 0
\end{equation}
\begin{equation} \label{eq:prel_qubo_stab}
	p_{2} \cdot \sum_{j < i} x_{i} x_{j} ((\prefers{m_i}{w_j}{w_i}) \cdot (\prefers{w_j}{m_i}{m_j})) 
	+ ((\prefers{m_j}{w_i}{w_j}) \cdot (\prefers{w_i}{m_j}{m_i})) = 0
\end{equation}
Equations \ref{eq:prel_qubo_noone_twice} - \ref{eq:prel_qubo_stab} are describing the problem SMP, completely.
The penalties, $p$, $p_1$ and $p_2$, introduce additional constraints into QUBO, like Glover et al \cite{Glover2018} proposed. The concrete assignments, needs a further look, how those terms evolve on the QUBO matrix, therefore 
\ref{eq:prel_qubo_noone_twice} - \ref{eq:prel_qubo_stab} will be reformulated so its more convenient to work with them programmatically.\\
\subsection{Reformulation for a more convenient usability} \label{sec:reformulation_for_better_usability}
Consider Equation \ref{eq:prel_qubo_noone_twice}, note that $(m_i = m_j) + (w_i = w_j)$ is $1$ 
if either $(m_i = m_j)$ \textit{or} $(w_i = w_j)$ is $1$, while $x_{i} \cdot x_{j} \cdot (i \neq j)$ will only be $1$
if $x_{i}$ and $x_{j}$ are $1$ and $i$ does not equal $j$.
This means, that the summand will only be one, if $x_{i}$ and $x_{j}$ are matched and a person is in both of those matches.
\footnote{$x_{i}$ is matched means that $x_{i} = 1$ so the edge (marriage) on the graph was selected by the QA} 
So if no one gets matched twice, this term will be zero.\\
Therefore equation \ref{eq:prel_qubo_noone_twice} can easy be obtained as \ref{eq:prel_qubo_noone_twice_matrix}. 
\footnote{Note that while in equation \ref{eq:prel_qubo_stab_matrix} the sum can either be $\{0,1,2\}$ so three cases are needed, while in \ref{eq:prel_qubo_noone_twice_matrix} only
	two are considered $\{0,1\}$, this holds, because $(m(i) = m(j)) + (w(i) = w(j))$ cannot be $2$ unless $(i = j)$, which is out ruled by $(i \neq j)$}
\begin{equation} \label{eq:prel_qubo_noone_twice_matrix}
	qubo[i][j] = \begin{cases}
	p: & (m_i = m_j \vee w_i = w_j) \land i \neq j \\
	0: & else
	\end{cases}
\end{equation}
To get the correct positions for equation \ref{eq:prel_qubo_max_count} on the QUBO matrix, it needs to be expanded:
\begin{equation*}
	\begin{split}
		(\sum_{i} x_{i} - n)^2 = \\
		(\sum_{i} x_{i})^2 - 2 n \sum_{i} x_{i} + n^2 = \\
		(\sum_{i} x_{i}^2 + \sum_{j < i} x_{i} x_{j}) - 2 n \sum_{i} x_{i} + n^2 = \\
		\sum_{j < i} x_{i} x_{j} - 2 (n-1) \sum_{i} x_{i} + n^2 = \\
	\end{split}
\end{equation*}
With that in mind, the assignment can be reviewed as \ref{eq:prel_qubo_max_count_matrix}. A offset of $n^2$, will remain from this reformulation.
\begin{equation}
	\label{eq:prel_qubo_max_count_matrix}
	qubo[i][j] = \begin{cases}
	- 2p_{1}(n-1) & : i == j \\
	p_{1} & : else \\
	\end{cases}
\end{equation}
Equation \ref{eq:prel_qubo_stab}, $((\prefers{m_i}{w_j}{w_i}) \cdot (\prefers{w_j}{m_i}{m_j})) 
+ ((\prefers{m_j}{w_i}{w_j}) \cdot (\prefers{w_i}{m_j}{m_i}))$ can like equation \ref{eq:prel_qubo_noone_twice} be reduced to a boolean formulation.
Note the this time the term can either get $0$, $1$ or $2$, dependent on the following conditions:
\begin{equation*}
	\begin{split}
		a = (\prefers{m_i}{w_j}{w_i}\; \land \;\prefers{w_j}{m_i}{m_j}) \\
		b = (\prefers{m_j}{w_i}{w_j}\; \land \;\prefers{w_i}{m_j}{m_i})
	\end{split}
\end{equation*}

\begin{itemize}
	\item if both a and b are false the term is $0$
	\item if either one of a or b are true the term is $1$
	\item if both are true, the term is $2$
\end{itemize}
Therefore, equation \ref{eq:prel_qubo_stab} can be obtained as \ref{eq:prel_qubo_noone_twice_matrix}
\begin{equation}
	\label{eq:prel_qubo_stab_matrix}
	qubo[i][j] = \begin{cases}
	2 p_{2}: & a \land b\\
	0: & \lnot a \land  \lnot b\\
	p_{2}: & else 
	\end{cases}
\end{equation}
Combining \ref{eq:prel_qubo_noone_twice_matrix}, \ref{eq:prel_qubo_max_count_matrix} and \ref{eq:prel_qubo_stab_matrix}, the element wise assignment of the
QUBO matrix can be obtained. 
\begin{center}
	\begin{algorithm}
		\KwData{$M, W$ with strictly ordered complete preference lists}
		\KwResult{A stable matching $M$}
		\For{$i\leftarrow 0$ \KwTo $n^2$} {
			\For{$j\leftarrow i$ \KwTo $n^2$} {
				\uIf{i = j}{
					qubo[i][j] += $-2p_{2}(n-1)$
				}
				\Else{
					// from equation \ref{eq:prel_qubo_noone_twice_matrix}\\
					\If{$m_i = m_j$} {
						qubo[i][j] += $p$\\
					}
					\If{$w_i = w_j$} {
						qubo[i][j] += $p$\\
					}
					// from equation \ref{eq:prel_qubo_stab_matrix}\\
					\If{$\prefers{m_i}{w_j}{w_i}\; \land \;\prefers{w_j}{m_i}{m_j}$} {
						qubo[i][j] += $p_2$\\
					}
					\If{$\prefers{m_j}{w_i}{w_j}\; \land \;\prefers{w_i}{m_j}{m_i}$} {
						qubo[i][j] += $p_2$\\
					}
				}
			}
		}
		\caption{QUBO creation for SMP}
		\label{algorithm:prel_qubo_creation}
	\end{algorithm}
\end{center}
\subsection{Penalty Assignment}\label{sec:smp_penaltie_assignment}
Consider the SMP problem of size $2$, with $M = \{m_0, m_1\}$ and $W = \{w_0, w_1\}$. The preferences are specified in 
\ref{table:smpinstance}.
\begin{figure}[H]
	\centering
	\begin{tabular}{c c c m{2cm} c c c}
		$m_0$: & $w_0$, & $w_1$ &   & $w_0$: & $m_0$, & $m_1$ \\
		$m_1$: & $w_0$, & $w_1$ &   & $w_1$: & $m_1$, & $m_0$ \\
	\end{tabular}
	\caption[SMP Instance]{A two persons SMP instance}
	\label{table:smpinstance}
\end{figure}
For the following problem, the resulting QUBO will look like this:
\[
	\begin{blockarray}{c c c c c}
		\begin{block}{c c c c c}
			& m_0, w_0 & m_0, w_1 & m_1, w_0 & m_1, w_1 \\
		\end{block}
		\begin{block}{c(c c c c)}
			m_0, w_0 & -2p_{1}(n-1) &  p+p_1 &  p+p_1 &  p_1 & \\
			m_0, w_1 & 0 &  -2p_{1}(n-1) &  p_2+p_1 &  p+p_1 & \\
			m_1, w_0 & 0 &  0 &  -2p_{1}(n-1) &  p+p_1 & \\
			m_1, w_1 & 0 &  0 &  0 & -2p_{1}(n-1) & \\
		\end{block}
	\end{blockarray}
\]
In general there is $-2p_1(n-1)$ assigned to the diagonal, and $p_1$ assigned to every other element. 
Note that if either one of the persons is equal in the interaction, $p$ is additionally assigned to the matrix.
For the interaction $(x_{(m_1, w_0)}, x_{(w_0, m_1)}$ $w_0)$ and $m_0$ would prefer each other over the current match, that
is assigned to them, therefore this field is assigned with $p_2$. \\
So an other way of viewing this formulation, is that on the diagonal, the current match is proposed, while on its line, its 
reviewed column wise against the other matchings. This thought brings up the review on the penalty assignment.\\
So for $p$, if there are any equal pairs, its not desired to match the Qbits $x_i$ and $x_j$, since that matching would not be stable at all.
The penalty must be larger then $2(n-1)p_1$ \ref{eq:prel_p} 
\begin{equation} \label{eq:prel_p}
	p = 2np_1 + 1 
\end{equation}
For $p_2$ holds the same intuition, but its sufficient that $p_2$ is larger then $p_1$ and not $2(n-1)p_1$ \ref{eq:prel_p2}.
\begin{equation} \label{eq:prel_p2}
	p_2 = p_1 + 1
\end{equation}
Since, both $p_2$ and $p$ are dependent from $p_1$ and $p_1$ is assigned to every element on the matrix, $p_1$ can be seen as a multiplicative factor 
and is arbitrarily set to $1$.
\begin{equation} \label{eq:prel_p1}
	p_1 = 1  
\end{equation}

\subsection{Energy of correct solutions}
At last, the desired energy output will be obtained. Its assumed with out the loss of generality,
that $n$ pairs will be matched, no one is matched twice and there are no unstable pairs.
So for the energy output of the QA, this means, that $n$ times $-2p_1(n-1)$ will be assigned to the diagonal.
For every element on the diagonal at index $i$, $i$ times $p_1$ will be added to the overall energy.
This can easily be visualized with the intuition from section \ref{sec:qubo}
Therefore the energy can be written as:
\begin{equation*}
	\begin{split}
		n \cdot -2p_1(n-1) + \sum_{i}^{n-1} i \cdot p_1 = \\ 
		-2p_1(n-1)n + p_1 \frac{n(n-1)}{2} = \\
		-p_1 (2(n-1)n - \frac{(n-1) n}{2}) = \\
		-p_1 (n-1)n (2- \frac{1}{2}) = \\
		-\frac{3}{2}p_1(n-1)n
	\end{split}
\end{equation*}
Taking the given penalty value from above $p_1 = 1$, the energy of a correct solution can be found in \ref{eq:prel_optimal_energy}.
\begin{equation} \label{eq:prel_optimal_energy}
	-\frac{3}{2} \cdot (n-1)n
\end{equation}
When solving the QUBO formulation, with \textit{Qbsolv}, this value can be passed as the target parameter and results in a huge computational speedup.
