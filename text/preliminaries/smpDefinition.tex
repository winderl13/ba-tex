\section{The Stable Marriage Problem (SMP)}
In this section the standard version of the stable marriage problem will be introduced and later, 
generalizations and their NP/APX-hardness will be discussed.
Last but not least two approximation algorithms will be reviewed.
\subsection{Introduction to the Stable Marriage Problem}
The stable marriage problem (short \textit{SMP}) describes the problem 
of finding a stable matching between two sets, males $M=\{m_{0}, m_{1}, ...\}$ and females $W=\{w_{0}, w_{1}, ...\}$, 
with size of $|M| = |W| = n$.\\
Both rank the other sex in a strictly ordered and complete preference lists, 
therefore every preference list has the lenght of $n$. 
A matching $M = \{(m_{i}, w_{j}), ...\}$ of size $n$ is considered as 
stable, if there exists no blocking pair p.\\
Let $M(m|w)$ describe the notation for $m|w$'s matching partner and let \prefers{m_{1}}{w_{1}}{w_{2}},
describe the notation that $m_{1}$ (strictly) prefers $w_{1}$ over $w_{2}$.\\
A pair $p=(m,w)$ is considered as blocking towards $M$, if \prefers{m}{w}{M(m)} 
and \prefers{w}{m}{M(w)}. So if there are no pairs matched, which both would be better of with somebody else, 
than their current match is stable.\footnote{Note, that this doesn't mean everybody is happy (in terms of being 
	matched to the most desired possibility available), furthermore this can be expressed, that everybody is 
	matched in a way he cannot complain for a good reason}\\
A Matching can be considered as stable if there are no blocking pairs towards m.
It is known, that this Problem can be solved in $O(n^2)$, by the Gale/Shapley-Algorithm (GS-Algorithm) \cite{Manlove2002}.\\
As outlined in algorithm \ref{algorithm:gsAlgorithm}, the algorithm let males (or females) propose the other sex, a 
proposal develops to a match, if either $w$ (or $m$) is free or prefers the current applicant over
their current match. In the second case, the previously matched partner becomes solo and needs
to propose to the other gender again. The algorithm stops, if all males (and therefore all females) are matched.\\
\begin{algorithm}[ht]
	\KwData{$M, W$ with strictly ordered complete preference lists}
	\KwResult{A stable matching $M$}
	\While{$\exists m$ (male), that can propose to a female}{
		let $w$ be the first woman on $m$'s preference list
		\eIf{$w$ is not matched}{
			$M[m] = w$ \qquad// match m to w
			}{
			\If{\prefers{w}{m}{M(w)}}{
				$M(w)$ is solo again\\
				$M[m] = w$ \qquad// match m to w
			}
		}
	}
	\caption{The Gale/Shapley Algorithm to find a stable matching \cite{Gale1962}} 
	\label{algorithm:gsAlgorithm}
\end{algorithm}
\subsection{Generalizations of SMP}
The basic GS-Algorithm only works for complete and strict preference lists, nevertheless this often does not suite
reality well.\\
Therefore manly three variants of the SMP developed, namely stable marriage problem with incomplete preference 
lists (SMI), the stable marriage problem with ties in its preference lists (SMT) and the combination of 
both stable marriage problem with ties and incomplete preference lists (SMTI).
\subsubsection[SMT]{SMT - Stable Marriage Problem with ties}
In the basic stable marriage problem, the preference lists must be ordered in a strict way.
Relaxing the rules, so that indifference is allowed (ties can occur) on preference lists, 
results in two different ways of describing preference and in three definitions of stability.\\
\textbf{Strict preference} is true for $(m_{0}, w_{1}, w_{2})$, if: $m_{0}$ prefers $w_{1}$
over $w_{2}$ and does not tie $w_{1}$ and $w_{2}$. Its denoted as \prefers{m_{0}}{w_{1}}{w_{2}}\\
\textbf{(Weak) preference} is true for $(m_{0}, w_{1}, w_{2})$, if: $m_{0}$ prefers $w_{1}$
over $w_{2}$ or both are tied on $m_{1}$'s preference list. The notation here is \prefersWeak{m_{0}}{w_{1}}{w_{2}}.\\
With that in mind, the three definitions for stability of a matching can be made the following\cite{Manlove2002}:
\begin{enumerate}
	\item \textit{weakly stable}: there is no pair, that would strictly prefer each other over their
	      partner in M.
	\item \textit{strongly stable}: there is no pair $(x,y)$, so that \prefers{x}{y}{M(x)} and \prefersWeak{y}{x}{M(y)}
	\item \textit{super stable}: there is no pair, that would (weakly) prefer each other over 
	      their current match.
\end{enumerate}    
Such a pair (described in the manner of weakly, strongly and super stability) is described as a blocking
pair if it exists, like in the SMP.\\
For a given SMT instance, a weakly stable matching always exists and can be found in $O(n^2)$, 
by breaking ties arbitrarily. A super stable matching may not exists but there is a Algorithm that can
find such a matching or report if there is none (in $O(n^2)$). A strongly stable matching can also 
be nonexisting and found in $O(n^3)$ \cite{Irving2008}.

\subsubsection[SMI]{SMI - Stable Marriage Problem with incomplete preference lists}
Incomplete preference lists do refer to the term, that not every person of the opposite
sex needs to be listed on a persons preference list. This results, in the fact that not every male
can be matched to every female \footnote{See the situation where $m$ has $w$ on his preference
list, but $w$ does not mention $m$. A match $(m,w)$ is therefore impossible}.
Therefore a acceptable pair, can be defined as followed.\\
\textbf{Acceptable pair:} A pair $(m,w)$ is acceptable, if $m$ has listed $w$ on his
preference list and $w$ has listed $m$.\\
In SMI a matching is stable if its stable in terms of SMP and no unacceptable pairs where matched.
\subsubsection[SMTI]{SMTI - Stable Marriage Problem with ties and incomplete preference lists}\label{sec:smti_description_section}
Combining the both generalizations from above, we obtain the stable marriage problem with ties and 
incomplete preference lists (SMTI).
Finding a weakly stable match for a given SMTI Problem, by breaking down ties arbitrarily (like in SMP),
will affect the size of the resulting match. This effect will be interesting for MAX-SMTI and therefore in the scope of this thesis
weakly stable SMTI will be reviewed as SMTI.
So a pair $(m,w)$ is considered as blocking towards a matching $M$, when the following takes account:
\begin{equation} \label{eq:def_max_smti_blocking}
	(free(m) \vee \prefers{m}{w}{M(m)}) \land \\
	(free(w) \vee \prefers{w}{m}{M(w)})
\end{equation}
Where $free(m|w)$ denotes if $m|w$ are matched to anyone in $M$. Note, that either if $\prefers{m}{w}{M(m)})$ or $free(m)$ 
is sufficient to create an unblocking pair towards ths matching $M$.\\
Consider the example from Table \ref{table:smtiinstance}, with: 
$$Males: m_{0}, m_{1}, m_{2}, m_{3}$$
$$Females: w_{0}, w_{1}, w_{2}, w_{3}$$ 
$$n = |Males| = |Females|$$ 
Its easy to review that 
$M_{1} = \{(m_{0}, w_{2}); (m_{2}, w_{0})\}$ is weakly stable (has no blocking pairs). Also 
$M_{2} = \{(m_{2}, w_{0}))\}$ can be reviewed as weakly stable. Note that the cardinality of the two stable
matchings differ in size.
\begin{figure}[H]
	\centering
	\begin{tabular}{c c c c m{2cm} c c c c}
		$m_{0}$: & ($w_{1}$ & $w_{2}$) & $w_{0}$ &   & $w_{0}$: & ($m_{3}$ & $m_{2}$) & $m_{0}$ \\
		$m_{1}$: & ($w_{2}$ & $w_{3}$) &         &   & $w_{1}$: & ($m_{2}$ & $m_{1}$) & $m_{3}$ \\
		$m_{2}$: & $w_{0}$  &          &         &   & $w_{2}$: & $m_{0}$  &          &         \\
		$m_{3}$: & ($w_{1}$ & $w_{0}$) &         &   & $w_{3}$: & ($m_{1}$ & $m_{3}$) &         \\
	\end{tabular}
	\caption[A SMTI Instance]{A SMTI Instance: the persons in the brackets are tied}
	\label{table:smtiinstance}
\end{figure}
This effect of possible indifference in the cardinality of the matchings does only occur in the weakly stability criteria.
\subsection[MAX-SMTI]{MAX-SMTI}\label{sec:maxsmti}
In the previous section was discussed, that breaking ties arbitrarily in SMTI will affect the size of the resulting match. 
Trying to find the matching with the largest cardinality, is a NP-hard optimization problem, referenced in the following by:
MAX-SMTI \cite{Irving2008}.\\
\\
Approximation algorithms in computer science are efficient algorithms which can approximate (mostly NP-hard) problems with 
a guaranteed minimum distance of their solution to the optimal one. 
APX is a class that includes problems, which have a polynomial time approximation algorithm and the optimality of the solution 
(approximation ratio) is bound by a constant.\\
To illustrate this approximation ratio, assume that for a APX-hard problem $v$ is the cost of the optimal solution. 
$v(y)$ is the cost of the result ($y$) of an approximation algorithm.
For maximization problems, this approximation factor computes by $\frac{v}{v(y)}$. For a minimization problem, it is $\frac{v(y)}{v}$.
So this factor is always $\geq 1$. In case the factor is one, the approximation algorithm finds the optimal solution.\\
It can be shown, that MAX-SMTI is APX-hard, with a lower bound on the approximation ratio of 21/19. \cite{PODHRADSKY2011}
\subsection{MAX-SMTI: Exact Solver} \label{sec:opt_solution}
The brute force optimal solution would be to calculate every matching for every possible arrangement of tie breaks 
and return the matching with the maximum cardinality. It is quite obvious that the execution of this algorithm would be
very exhausting even for small instances.\\
Therefore an other approach was developed by Roth et al. \cite{Roth} using integer linear programming:
\begin{equation}\label{eq:max_smti_lp}
	\begin{aligned}
		  & Maximize: &   & \sum_{i=0}^n\sum_{j=0}^n x_{i,j}                                          \\
		  & s.t:      &   & \quad\quad\quad\quad \forall w: \sum_{j} x_{j,w} \le 1                    \\
		  &           &   & \quad\quad\quad\quad \forall m: \sum_{i} x_{i,m} \le 1                    \\
		  &           &   & \forall (m, w) \in A: \sum_{j} x_{m,j} + \sum_{i} x_{i,w} - x_{m,w} \ge 1 \\
		  &           &   & \quad\quad\quad\quad \forall (m, w) \not\in A x_{m,w}= 0                  \\
		  &           &   & \quad\quad\quad\quad \forall (m, w) \in \{0,1\}                           \\
	\end{aligned}
\end{equation}
In this thesis this LP-approach will be reviewed as MAX-SMTI-LP.\\
\subsection{MAX-SMTI: Approximation Algorithms} \label{sec:apx_algorithms}
As mentioned in \ref{sec:maxsmti} MAX-SMTI is APX-hard with a approximation factor of 21/19. So therefore many approximation algorithms exist.
Podhradsky et al \cite{PODHRADSKY2011} reviewed and implemented many of those algorithms. For the general SMTI Problem, the following algorithms are 
feasible: RandBrk, SHIFTBRK, IMY, Kiraly2, McDermid, Paluch \cite{PODHRADSKY2011}. In figure \ref{fig:algorithms_max_smti} 
it can be seen, that Kiraly2 and SHIFTBRK have a high accuracy while Kiraly2 runs clearly faster then SHIFTBRK and SHIFTBRK 
is by far the slowest algorithm, among all others.\footnote{Also McDermid and Paluch would have a equally accuracy, but Kirialy2 was arbitrarily taken as a example}
So therefore in this section the algorithms Kiraly2 and SHIFTBRK will be described next.
\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=0.75\linewidth]{pictures/alg_comparison.png}
		\caption[Approximation algorithms MAX-SMTI]{Comparison of the runtime and accuracy of different approximation algorithms for MAX-SMTI \cite{PODHRADSKY2011}}
		\label{fig:algorithms_max_smti}
	\end{center}
\end{figure}
\subsubsection{Kiraly2}
Kirialy et al \cite{Kiraly2008} introduced two simple and linear approximation algorithms for MAX-SMTI.
The first, a linear $\frac{3}{2}$-approximation algorithm, works for SMTI instances, with men strict preference lists. 
This was followed by a second algorithm, for general SMTI problems with a approximation ratio of $\frac{5}{3}$, based
on the first men strict algorithm. See \cite{PODHRADSKY2011} for a detailed description and implementation.
\subsubsection{SHIFTBRK}
Halld\`orsson et al \cite{Halldorsson2003} introduced SHIFTBRK (algorithm \ref{algorithm:SHIFTBRK}), as an approximation algorithm, with a ratio of $\frac{2}{1 + L^2}$, where $L$ is 
the length of the longest tie among all ties in the preference lists. So the approximation ratio decreases within the increase of the length of the longest tie.
The algorithm, is based on iteratively breaking and shifting ties and can solve the resulting SMI instances via the GS-Algorithm.\\
Consider a shift as the following:\\
Let $(p_1, p_2, ...p_k)$, be a tie of length $k$ which is arbitrarily broken. 
A SHIFT permutes this tie, by adding the first element as last:
$$
SHIFT((p_1, p_2, ...p_k)) = (p_2, ...p_k, p_1)
$$
In order to give a instance, reconsider the SMTI instance from section \ref{sec:smti_description_section}. There 
$m_0$ has a tie $(w_{1} w_{2})$. 
This tie can be either broken by putting $w_{1}$ in front of $w_{2}$ on $m_0$s preference list or putting $w_{2}$ in front of $w_{1}$.
The resulting SHIFT of $w_1, w_2$ would be $w_2, w_1$.
\begin{algorithm}[ht]
	\KwData{a SMTI instance I}
	\KwResult{A stable matching $M$}
	$I_{1,1}$ break all ties in I arbitrarily \\
	L = lenght of the longest tie \\
	\For{$i \rightarrow 2, ... L$}
	{
		$I_{i,1}$ = $SHIFT_w(I_{i-1, 1})$
	}
	\For{$i \rightarrow 2, ... L$}{
		\For{$j \rightarrow 2, ... L$}{
			$I_{i,j}$ = $SHIFT_m(I_{i, j-1})$
		}
	}
	\For{$i \rightarrow 1, ... L$}{
		\For{$j \rightarrow 1, ... L$}{
			s = $GS(I_{i,j})$ \\
			\If{s.size < opt.size} {
				opt = s
			}
		}
	}
	\caption{The SHIFTBRK algorithm, for MAX-SMTI\cite{PODHRADSKY2011}} 
	\label{algorithm:SHIFTBRK}
\end{algorithm}