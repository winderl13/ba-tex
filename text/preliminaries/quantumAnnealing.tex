\section{Quantum Computing}
The term of quantum computing can be subdivided in two different models of computation: The quantum gate model and adiabatic quantum computing.
The difference can be made by its analog and discrete nature.
\subsection{Quantum Gate Model}
The universal gate model (quantum gate model), describes a model of quantum computation, 
in which Q-Bits circuits are used either in row or parallel. Each Q-Bit can have the quantum mechanic properties 
discussed in the previous section. Because of superposition, one register can have $2^{n}$ states, while a
turing machine would have to update those $2^{n}$ registers. This \textit{quantum parallelism} is the main reason
for the hope for a speedup of the computation time \cite{McGeoch2014}.
\subsection{Adiabatic Quantum Computing} \label{sec:aqc}
Adiabatic quantum computers (AQC) cannot be seen in a discrete way, furthermore the circuit can be viewed as a 
quantum particle system, which is interacting with itself (with the state of one particle describing one
Q-Bit). \\
An adiabatic quantum algorithm is designed to solve the following problem:
Given an objective function $f: \mathbb{D}^n \rightarrow \mathbb{R}$ with a discrete domain 
$x = \{x_{1}, x_{2},... x_{n}\}$ with $x_{i} \in \mathbb{D}$, find the assignment of $x$, that minimizes $f$.
So a AQC assigned with a quantum algorithm evolves over time $t$ to the correct solution (assignment) to $f$. 
It can be expressed by $H(t)$, a time varying Hamiltonian, which itself consist out of three components \cite{McGeoch2014}:
\begin{itemize}
	\item A easy to find initial hamiltonian $H_{I}$ defined by the user
	\item A hard to find problem hamiltonian $H_{P}$ that encodes the objective function
	\item An adiabatic evolution path $s(t)$ which describes the transition from $H_{I}$ to $H_{P}$ over time
\end{itemize}
Therefore $H(t)$ can be obtained as:
$$
H(t) = H_{I} \cdot (s(t)-1) + H_{P} \cdot s(t)
$$
So intuitively the problem is encoded on a swarm of subatomic particles, they evolve over time and after a certain amount of time, 
the solution will be encoded on the particles.\\
The adiabatic theorem states now, that if the process of the particles (Q-Bits) evolve 'slowly enough' and 
$H_{I}$ is in ground state, $H_{P}$ will be in ground state, too.
\footnote{The ground state of $H_{P}$ corresponds to the minimum assignment of $f$}
The upper bound of the evolution time also follows out of the adiabatic theorem. See Farhi et al. \cite{Farhi2000} for a detailed derivation.\newpage
\subsection{Quantum Annealing}
Quantum Annealing (QA) is a heuristic class of algorithmic methods and tools to solve (mostly) NP-hard 
optimization problems. The QA algorithm was developed by Kadowaki et al. \cite{Kadowaki1998} inspired by simulated annealing.\\\\
\textbf{Simulated Annealing}\\
Simulated annealing is a statistical meta heuristic approach, to approximate the global optimum of a given problem function 
$f: \mathbb{D}^n \rightarrow \mathbb{R}$.\\
The algorithm carries out a random walk trough the solution space, by randomly choosing the next neighbor. The neighbor will be visited, either if 
he has a lower cost (value of $f$) or by a certain probability, ruled by a temperature $T$. While the random walk takes place, $T$ decreases continually.
Because of the introduced temperature, the algorithm can take some uphill moves, 
to possibly moving out of a local optimum and find the global optimum. 
See \ref{fig:simulatedAnnealing} for a plausible visualization.\cite{McGeoch2014}\\
\begin{figure}[h]
\begin{center}
		\includegraphics[width=0.5\linewidth]{pictures/SimulatedAnnealingExample.png}
		\caption[Simulated Annealing]{Simulated Annealing on a one dimensional objective function \cite{Ghasemalizadeh2016}}
		\label{fig:simulatedAnnealing}
	\end{center}
\end{figure}\\
\textbf{Quantum Annealing}\\
Quantum annealing makes kind of the same approach as simulated annealing, 
but instead of using the temperature to take an uphill move, it uses the \textit{transverse field coefficient} $\Gamma$ to control traversibility through the 
landscape. In resemblance to SA, $\Gamma$ starts at a high value and decreases over time, while the random walk takes place.\\
After the random walk, QA will most likely end up in the global optimum of the objective function.
QA can also be expressed using hamiltonian's, where $\Gamma$ is described by a transverse filed hamiltonian $H_{\Gamma}$ and the optimal
solution can be expressed by the ground state of $H_{F}$. Therefore, like in AQC a time dependent hamiltonian can be derived: \cite{McGeoch2014}
$$H(t) = H_{\Gamma}\cdot\Gamma(t) + H_{F}$$\\
The resemblance, between AQC and QA according to their hamiltonian's is obvious, however the difference between AQC and QA can 
be made in the way QA and AQC do computation. While AQC derives a bound from the probability of finishing in the ground state 
(adiabatic theorem), QA derives it bound from converging into the correct solution.\cite{McGeoch2014}\\
Because AQC and QA do not differ in the way problems are formulated and the scope of this thesis will be about formulating 
problems for AQC or QA, those terms will be used interchangeable. 
\subsection{Problem Formulations for Adiabatic Devices}
In this section the two ways of formulating problems for AQC, namely Quadratic Unconstrained Binary Optimization (QUBO) and the Ising Model, will be
introduced.
\subsubsection{The Ising Model}
The Ising model describes the ferromagnetism of $n$ spins $s = \{s_{1},...s_{n}\}$ ordered on a lattice. 
Each spin can either be $-1$ or $1$; $s_{i} \in \{-1;1\}$.
The energy of the system can be derived as equation \ref{eq:ising_sum}. There $h_i$ describe the external forces, applied
to the particle $s_i$ and $J_{ij}$ describes the interaction force between the particles $s_i$ and $s_j$. \cite{McGeoch2014}
\begin{equation}
	H = - \sum_{i} h_{i} s_{i} + \sum_{i < j} J_{ij}s_{i}s_{j}
	\label{eq:ising_sum}
\end{equation}
A ising spin glass is the NP-hard optimization problem of finding the assignment of spins, that minimizes $H$ \cite{McGeoch2014, Lucas2013}.
\begin{figure}[h]
	\begin{center}
		\tikzset{vertex/.style = {shape=circle,draw,minimum size=3em}}
		\tikzset{edge/.style = {-}}
		\begin{tikzpicture}[scale=1.2, every node/.style={transform shape}]
			\node[vertex] (a) at  (-2,0) {0.5};
			\node[vertex] (b) at  (2,0) {-0.3};
			\draw[edge] (b) to (a) node[midway, above] {0.4};
		\end{tikzpicture}
	\end{center}
	\caption{An example ising spin glass} \label{fig:example_ising}
\end{figure}\\
Figure \ref{fig:example_ising} shows such a example for a ising spin glass, also used by McGeoch et al \cite{McGeoch2014}. The two spins $s_1$ and $s_2$, have the values
$0.5$ and $-0.3$. Both are connected by an interaction with the value $0.4$.
Different values can be computed by equation \ref{eq:ising_sum}, towards:
\begin{equation*}
	\begin{align}
		H(+1, +1) & = -(1 \cdot 0.5 + 1 \cdot -0.3) + 1 \cdot 1 \cdot 0.4 = 0.2 \\
		H(+1, -1) & = -(1 \cdot 0.5 + 1 \cdot 0.3) - 1 \cdot 1 \cdot 0.4 = -1.2 \\
		H(-1, +1) & = -(-1 \cdot 0.5 - 1 \cdot 0.3) - 1 \cdot 1 \cdot 0.4 = 0.4 \\
		H(-1, -1) & = -(-1 \cdot 0.5 + 1 \cdot 0.3) + 1 \cdot 1 \cdot 0.4 = 0.2 
	\end{align}
\end{equation*}
So the minimum energy can be obtained by the assignment, $(1, -1)$ with a value of $-1.2$. 
It is quite feasible, that finding the minimum energy for such a
ising spin glass can be NP-hard, because of the vast combinations of assignments.
\subsubsection{Quadratic Unconstrained Binary Optimization (QUBO)}\label{sec:qubo}
Quadratic Unconstrained Binary Optimization (short: QUBO), is a NP-hard pattern matching technique common in machine learning.
A QUBO problem consists out of an upper triangular matrix $Q$, with a shape $(n,n)$ and a bit vector $x = \{x_{1},..x_{n}\}$, of the length $n$.
The goal is now to minimize the Term: $xQx^T$:
\begin{equation}
	\min x Q x^T
	\label{eq:qubo_matrix}
\end{equation}
An other way to formulate the QUBO would be using sums:
\begin{equation}
	\min \sum_{i,j} x_{i}\cdot Q_{i,j}\cdot x_{j} 
	\label{eq:qubo_sum}
\end{equation}
Its easy to see the resemblance between \ref{eq:ising_sum} and \ref{eq:qubo_sum}, where the only difference occurs in the definition of spins or 
variables. Overall for optimization problems the QUBO-Model can be more convenient, since it connects to the bit standard of computers.\\
\\
\textbf{Simplification of matrix multiplication}\\
Since the computation of the QUBO Problem $xQx^T$ can get very unhandy for larger matrices, another way to view the calculation of $xQx^T$ is proposed.\\
Since $x$ is a bit vector, $xQx^T$ can be viewed as adding a grid above the $Q$ matrix on every position $i$ where $x_{i} = 1$. The Term $xQx^T$ 
can be computed by summing up every element where the grid intersects itself.\\
Consider the example, with \ref{eq:def_x_example1} and \ref{eq:def_q_example1} defining an arbitrary chosen $x$ and $Q$.
\begin{equation}\label{eq:def_x_example1}
	x=\left(\begin{matrix}
	0 \\ 1 \\ 1 \\ 0
	\end{matrix}\right)\\
\end{equation}
\begin{equation}\label{eq:def_q_example1}
	Q = \left(\begin{matrix}
	-2 &  9 &  9 &  2 & \\
	0 &  -2 &  0 &  9 & \\
	0 &  0 &  -2 &  9 & \\
	0 &  0 &  0 &  -2 & \\
	\end{matrix}\right)
\end{equation}
For debug purposes the goal is now to compute the matrix product $xQx^T$ \ref{eq:def_x_Q_x_mulipilcation}.
\begin{align} \label{eq:def_x_Q_x_mulipilcation}
	\begin{split}
	xQ = \left(\begin{matrix}
	-2\cdot0 + 9\cdot1 + 9\cdot1 + 2\cdot0 \\ 
	0\cdot0 -2\cdot1+0\cdot1 +9\cdot0\\
	0\cdot0+0\cdot0-2\cdot1+9\cdot0\\
	0\cdot0 +0\cdot0+0\cdot0-2\cdot0\\
	\end{matrix}\right) = 
	\left(\begin{matrix}
	18\\ 
	-2\\
	-2\\
	0\\
	\end{matrix}\right) \\
	xQx^T =
	\left(\begin{matrix}
	18\\ 
	-2\\
	-2\\
	0\\
	\end{matrix}\right)\cdot
	\left(\begin{matrix}
	0 & 1 & 1 & 0 
	\end{matrix}\right) = 18\cdot 0+ 1\cdot (-2) + 1\cdot (-2) + 0\cdot 0 = -4
	\end{split}
\end{align}
It is obvious that this is unhandy even for smaller instances, now consider \ref{eq:q_grid_example}. With just summing up the intersection points, the same
result can be obtained faster: $-2 + (-2) + 0 = -4$

\tikzset{
	style orange/.style={
		fill=none,
		set border color=orange!80!red!60,
	},
	hor/.style={
		above left offset={-0.15,0.31},
		below right offset={0.15,-0.125},
		#1
	},
	ver/.style={
		above left offset={-0.15,0.4},
		below right offset={0.4,-0.12},
		#1
	}
}

\begin{equation}\label{eq:q_grid_example}
	Q = \left(\begin{matrix}
	-2 &  \tikzmarkin[ver=style orange]{c}9 &  \tikzmarkin[ver=style orange]{c1}9 &  2 & \\
	\tikzmarkin[hor=style orange]{r}   0 &  -2 &  0 &  9 \tikzmarkend{r}\\
	\tikzmarkin[hor=style orange]{r1} 0 &  0 &  -2 &  9 \tikzmarkend{r1}\\
	0 &  \tikzmarkend{c}0 &  \tikzmarkend{c1}0 &  -2 & \\
	\end{matrix}\right)
\end{equation}
This intuition was used to compute the QUBO matrices, by hand and even when not directly addressed, its often used
implicitly through out this paper.