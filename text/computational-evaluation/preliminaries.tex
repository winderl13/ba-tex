\chapter{Computational evaluation}\label{cap:4}
In this chapter a computational evaluation of \textit{Qbsolv} against the approximation algorithms and the linear programming
version for the optimal solution will be made. The scale of the computational time and the 
accuracy of \textit{Qbsolv} for larger instances will be measured.
\section{Preliminaries}
\subsection{Creation of Dataset}
Podhradsky et al. \cite{PODHRADSKY2011} and Delorme et al. \cite{Delorme2019} provided datasets
for their computational evaluation. But since \textit{Qbsolv} tends to get very inaccurate for instances $n \geq 20$
(see Section \ref{sec:inaccuracity_qbsolv}), it was not feasible to use those datasets, 
because the matching sizes were clearly above 20. \\
In order to store one SMTI instance, three JSON-files where used: 'females_pref.json', 'males_pref.json' and 'header.json'.
The files 'females_pref.json' and 'males_pref.json' store the males and females preferences. Examples can be found in \ref{list:fem_pref} and \ref{list:m_pref}.
\footnote{Note that behind every pair of keys of males and females $(m,w)$ the rank of $w$ on $m$s preference list can be found.}
Using the JSON standard, instead the format used by Podhradsky et al. \cite{PODHRADSKY2011} and Delorme et al. \cite{Delorme2019}
had the huge advantage, that file I/O was much easier and it was not necessary to write a own method to parse those files. On top
since, the preferences where stored in python dictionaries, the complexity to get a statement about preferences of two persons, is in $O(1)$.
The 'header.json' file was used to store metadata (min, max or mean preference length ect.).\\
\begin{center}
	\begin{tabular}{p{0.4\textwidth}p{.4\textwidth}}
		\begin{minipage}{.4\textwidth}
		\lstinputlisting[language=json,caption=Females preferences, label={list:fem_pref}]{pictures/females_pref.json}
		\end{minipage}
		  &   
		\begin{minipage}{.4\textwidth}
		\lstinputlisting[language=json, caption=Males preferences, label={list:m_pref}]{pictures/males_pref.json}
		\end{minipage}
	\end{tabular}
\end{center}
All males and females, where given uniform names, for practical reasons. 
By knowing the matching size, with a few lines of python code males and females could be computed for a instance (See \ref{lst:python_males_females_creation}).
Therefore it was not necessary to store the males and females.\\
\begin{minipage}{\linewidth}
        \hspace{0.5 cm}
	\begin{lstlisting}[language=Python, caption=Creation of males and females with python, label={lst:python_males_females_creation}]
                m = ["M" + str(index) for index in range(size)]
                w = ["W" + str(index) for index in range(size)]\end{lstlisting}
\end{minipage}
To create the dataset, two probabilities were introduced: $p_1 \in (0;1]$ and $p_2 \in (0;1]$. $p_1$ controls the probability, that an element is in a preference list and 
$p_2$ controls the tie density.
With those two probabilities in mind, the algorithm based on \cite{VanHarmelen2002} to create a SMTI dataset can be obtained. 
Algorithm \ref{algorithm:set_creation} outlines the creation of one preference list.
While a instance creation, this is done for males and females, the header is computed and the instance is returned.
\begin{algorithm}[ht]
	\KwData{the probabilities $p_1$, $p_2$ and the matching size $n$}
	\KwResult{A preference list for a SMTI instance}
        // Persons are either man or woman \\ 
        // other persons are the opposite gender of persons \\
        persons_preference = {} \\
        \For{$person \rightarrow persons$ } {
                pref_size = 1 \\
                \For{$i \rightarrow 1,..len(n)$}
                {
                        \If{rand(0,1) < $p_1$} {
                                pref_size = pref_size + 1
                        }
                }
                shuffle(other_persons)
                persons_pref[person] = {other_p: index for index, other_p in (other_persons_1[:pref_size])}
                last_person = other_persons_1[0]
                \For{$other_person \rightarrow persons_pref[person]$}
                {
                        \If{rand(0,1) < $p_2$} {
                                persons_pref[person][other_person] =  persons_pref[person][last_person]
                        }
                        last_person = other_person\\
                }

        }
	\caption{Creation of a SMTI preference list} 
	\label{algorithm:set_creation}
\end{algorithm}
\subsection{Time measurement}
The package \textit{timeit}, is used to calculate the execution time of one expression, 
because it is lightweight and flexible \cite{TimeitBib}.
\textit{Timeit} takes two strings and one optional integer as arguments. The first argument \textit{smtm}, takes the statement, 
of which the execution time should be evaluated.
The second argument setup, gets called before the execution takes place and can be feeded with code snippets to prepare the statement under test. 
The third optional argument for \textit{timeit}, named number, defines how often the statement get executed. The default value is $1.000.000$.
To measure the matching algorithms, it was sufficient to set this number parameter to $10$.\\
\begin{minipage}{\linewidth}
        \begin{lstlisting}[language=Python, caption=Measurement of math.sqrt with timeit]
                timeit('math.sqrt(2)', 'import math', number=1000)\end{lstlisting}   
\end{minipage}
\subsection{Realization of the state of the art algorithms}
To compare the QUBO formulation and \textit{Qbsolv} to state-of-the-art algorithms, 
the algorithm MAX-SMTI-LP, SHIFTBRK and Kirialy2 discussed in section \ref{sec:apx_algorithms} and \ref{sec:opt_solution}, 
were implemented in python. They are based on Podhradskys et al. \cite{PODHRADSKY2011} implementations.
In the following, there will only be a short review towards the framework that was used for linear programming, named \textit{Ortools}.
Also the parameters which were used for \textit{Qbsolv} are stated and explained.
\subsubsection{Optimal solution with linear programming}
For linear programming Googles \textit{Ortools} is used as a solver. \textit{Ortools} is written in C++, 
but also provides a python wrapper. This wrapper creates a \textit{Solver} object, from which then
the variables and constraints are obtained. Note the short example from the \textit{Ortools} documentation \cite{GoogleOrtools}.
Here \textit{Ortools} is used in order to solve equation \ref{eq:example_ortools}, a short example for a mixed integer program.
\begin{equation}\label{eq:example_ortools}
	\begin{aligned}
		  & Maximize: &   & x + 10 \cdot y    \\
		  & s.t:      &   & x + 7 y \leq 17.5 \\
		  &           &   & x +  \leq 3.5     \\
	\end{aligned}
\end{equation}
\begin{minipage}{\linewidth}
	\begin{lstlisting}[language=Python, label={fig:ortools_solver}, caption={Solving equation \ref{eq:example_ortools} with \textit{Ortools} \cite{GoogleOrtools}}]
        # Create the mip solver with the CBC backend.
        solver = pywraplp.Solver('simple_mip_program', 
            pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)

        # x and y are integer non-negative variables.
        infinity = solver.infinity()
        x = solver.IntVar(0.0, infinity, 'x')
        y = solver.IntVar(0.0, infinity, 'y')

        # x + 7 * y <= 17.5.
        solver.Add(x + 7 * y <= 17.5)
        # x <= 3.5.
        solver.Add(x <= 3.5)

        solver.Maximize(x + 10 * y)

        result_status = solver.Solve()
        # The problem has an optimal solution.
        assert result_status == pywraplp.Solver.OPTIMAL
	\end{lstlisting}   
\end{minipage}
In analogy to listing \ref{fig:ortools_solver}, the linear program for MAX-SMTI gets cast. 
Since in equation \ref{eq:max_smti_lp} for all $(m,w)$ it needs to be ensured that either $\sum_{j} x_{m,j} + \sum_{i} x_{i,w} - x_{m,w} \ge 1$
or $x_{m,w}= 0$ parsing the term has a complexity of $O(n^3)$, which is therefore $n$ times faster then the upper bound of 
QUBO-MAX-SMTI (which is $O(n^4)$).
\subsection{\textit{Qbsolv} Parameters}
\textit{Qbsolv} provides the user the option to set different parameters. 
For QUBO-MAX-SMTI and QUBO-SMP the following were relevant \cite{DwaveQbsolv}:
%\setlist{nolistsep}
\begin{itemize}[noitemsep]
	\item \textit{num_repeats}: determines the number of times the main loop is repeated, \\default: $50$
	\item \textit{algorithm}: which algorithm \textit{Qbsolv} uses, of: ENERGY_IMPACT or \\
              SOLUTION_DIVERSITY, default: ENERGY_IMPACT
	\item \textit{solver_limit}: the maximum sizes of a sub-QUBO; No default value was provided
\end{itemize}
In order to solve QUBO-MAX-SMTI, those parameters where set towards the following:
\begin{itemize}[noitemsep]
	\item \textit{num_repeats}: $50$, but was varied for different calculations
	\item \textit{algorithm}: SOLUTION_DIVERSITY, in order to find as many solutions as possible
	\item \textit{solver_limit}: the QUBO size
\end{itemize}
